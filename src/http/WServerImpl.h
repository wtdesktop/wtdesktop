// This may look like C code, but it's really -*- C++ -*-
/*
 * Copyright (C) 2008 Emweb bvba, Kessel-Lo, Belgium.
 *
 * See the LICENSE file for terms of use.
 */

#ifndef HTTP_SERVERIMPL_HPP
#define HTTP_SERVERIMPL_HPP

#include "Wt/WServer"

#include <string>

#include "Connection.h"
#include "Server.h"
#include "Configuration.h"
#include "../web/Configuration.h"
#include "WebController.h"
#include "HTTPStream.h"

#ifdef WT_THREADED
#ifdef BOOST_ASIO
typedef boost::thread thread_t;
#else
typedef asio::thread thread_t;
#endif
#endif

namespace Wt {

struct WServerImpl {
  WServerImpl(const std::string& wtApplicationPath,
	      const std::string& wtConfigurationFile)
  : wtConfiguration_(wtApplicationPath, wtConfigurationFile,
		     Wt::Configuration::WtHttpdServer,
		     "Wt: initializing built-in httpd"),
    webController_(wtConfiguration_, &stream_),
    serverConfiguration_(wtConfiguration_.logger()),
    server_(0)
  { }

  Configuration wtConfiguration_;
  HTTPStream    stream_;
  WebController webController_;

  http::server::Configuration  serverConfiguration_;
  http::server::Server        *server_;
#ifdef WT_THREADED
  thread_t **threads_;
#endif
};

}

#endif
