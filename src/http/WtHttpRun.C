// This may look like C code, but it's really -*- C++ -*-
/*
 * Copyright (C) 2008 Emweb bvba, Kessel-Lo, Belgium.
 *
 * See the LICENSE file for terms of use.
 */

#include "Wt/WServer"
#include "Wt/WApplication"

#include <iostream>
#include <string>

#if !defined(_WIN32)
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>
#endif // !_WIN32

#ifdef WT_THREADED
#ifdef BOOST_ASIO
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#else  // BOOST_ASIO
#include <asio/thread.hpp>
#endif // BOOST_ASIO
#endif // WT_THREADED

#include <boost/bind.hpp>

#include "Connection.h"
#include "Server.h"
#include "WServerImpl.h"
#include "Configuration.h"
#include "../web/Configuration.h"
#include "WebController.h"
#include "HTTPStream.h"

#ifdef WT_THREADED
#ifdef BOOST_ASIO
typedef boost::thread thread_t;
#else
typedef asio::thread thread_t;
#endif
#endif
namespace {
  static std::string getWtConfigXml(int argc, char *argv[])
  {
    std::string wt_config_xml;
    Wt::WLogger stderrLogger;
    stderrLogger.setStream(std::cerr);
    
    http::server::Configuration serverConfiguration(stderrLogger, true);
    serverConfiguration.setOptions(argc, argv, WTHTTP_CONFIGURATION);
    
    return serverConfiguration.configPath();
  }

}

namespace Wt {
int WRun(int argc, char *argv[], ApplicationCreator createApplication)
{
  try {
    WServer server(argv[0], getWtConfigXml(argc, argv));
    try {
      server.setServerConfiguration(argc, argv, WTHTTP_CONFIGURATION);
      server.addEntryPoint(Application, createApplication);
      if (server.start()) {
	int sig = WServer::waitForShutdown();
	server.impl()->serverConfiguration_.log("notice")
	  << "Shutdown (signal = " << sig << ")";
	server.stop();
      }

      return 0;
    } catch (std::exception& e) {
      server.impl()->serverConfiguration_.log("fatal") << e.what();
      return 1;
    }
  } catch (Wt::WServer::Exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  } catch (std::exception& e) {
    std::cerr << "exception: " << e.what() << std::endl;
    return 1;
  }
}

}
