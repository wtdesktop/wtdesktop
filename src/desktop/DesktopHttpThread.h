// This may look like C code, but it's really -*- C++ -*-
/*
 * Copyright (C) 2009 Emweb bvba, Kessel-Lo, Belgium.
 *
 * All rights reserved.
 */

#ifndef DESKTOP_HTTP_THREAD_HPP
#define DESKTOP_HTTP_THREAD_HPP

#include <QThread>
#include "QtProtector.h"
#include <WApplication>

namespace Wt {
class DesktopHttpThread : public QThread {

  Q_OBJECT
    
  public:
    
  DesktopHttpThread(int argc, char *argv[], ApplicationCreator createApplication) :
    argc_(argc), argv_(argv), createApplication_(createApplication), port_(0) { ; };
    
  ~DesktopHttpThread() { ; };
  unsigned int port();
  
  Q_SIGNALS:
    void httpThreadReady(unsigned int port);
  
  protected:
    void run();
    
  private:
    int argc_;
    char** argv_;
    ApplicationCreator createApplication_;
    unsigned int port_;
};

}

#endif
