#ifndef DESKTOP_MAIN_WINDOW_HPP
#define DESKTOP_MAIN_WINDOW_HPP

#include <QMainWindow>

class QWebView;
class QWebFrame;

class DesktopMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    DesktopMainWindow();
    ~DesktopMainWindow();

protected slots:
    void loadApp( unsigned int port );
    void adjustTitle();
    void setProgress(int p);
    void finishLoading(bool);
    void print();
    void printRequested(QWebFrame *frame);

private:
    QWebView *view_;
    int progress_;
};

#endif
